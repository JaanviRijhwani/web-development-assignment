const express = require('express');
const app = express();

// Function to check if a number is prime
function isPrime(num) {
    if (num <= 1) {
        return false;
    }

    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }

    return true;
}

// Generate the HTML page
function generateHTMLPage(number, primes) {
    let html = `
    <html>
      <head>
        <title>Prime Numbers</title>
      </head>
      <body>
        <h1>Prime Numbers till ${number}</h1>
        <ul>
  `;

    primes.forEach((prime) => {
        html += `${prime}<br>`;
    });

    html += `
        </ul>
      </body>
    </html>
  `;

    return html;
}

// HTTP GET endpoint to retrieve prime numbers
app.get('/primes', (req, res) => {
    const number = parseInt(req.query.number);

    if (isNaN(number)) {
        return res.status(400).send('Invalid number');
    }

    const primes = [];
    for (let i = 2; i <= number; i++) {
        if (isPrime(i)) {
            primes.push(i);
        }
    }

    const htmlPage = generateHTMLPage(number, primes);
    res.send(htmlPage);
});

// Start the server
app.listen(3000, () => {
    console.log('Server is running on http://localhost:3000');
});
