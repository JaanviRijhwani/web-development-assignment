import logo from './logo.svg';
import './App.css';

import React, { useState } from 'react';

const App = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [dob, setDOB] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setPhone] = useState('');
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    let isValid = true;
    const newErrors = {};

    if (name.trim() === '') {
      newErrors.name = 'Name is required';
      isValid = false;
    } else if (!/^[a-zA-Z\s]+$/.test(name)) {
      newErrors.name = 'Name should not contain special characters';
      isValid = false;
    }

    if (email.trim() === '') {
      newErrors.email = 'Email is required';
      isValid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      newErrors.email = 'Invalid email format';
      isValid = false;
    }

    if (dob.trim() === '') {
      newErrors.dob = 'DOB is required';
      isValid = false;
    } else if (!/^(\d{2})-(\d{2})-(\d{4})$/.test(dob)) {
      newErrors.dob = 'Invalid DOB format (DD-MM-YYYY)';
      isValid = false;
    }

    if (address.trim() === '') {
      newErrors.address = 'Address is required';
      isValid = false;
    }

    if (phone.trim() === '') {
      newErrors.phone = 'Phone number is required';
      isValid = false;
    } else if (!/^[1-9]\d{9}$/.test(phone)) {
      newErrors.phone = 'Invalid phone number format';
      isValid = false;
    }

    setErrors(newErrors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validateForm()) {
      // Handle form submission logic here
      console.log('Form submitted successfully!');
      // Reset form fields
      setName('');
      setEmail('');
      setDOB('');
      setAddress('');
      setPhone('');
      setErrors({});
    }
  };

  return (
      <form onSubmit={handleSubmit}>
        <div>
          <label>Name:</label>
          <input
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
          />

        </div>
        {errors.name && <span class="error-red">{errors.name}</span>}
        <div>
          <label>Email:</label>
          <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
          />

        </div>
        {errors.email && <span class="error-red">{errors.email}</span>}
        <div>
          <label>DOB:</label>
          <input
              type="text"
              value={dob}
              onChange={(e) => setDOB(e.target.value)}
          />

        </div>
        {errors.dob && <span class="error-red">{errors.dob}</span>}
        <div>
          <label>Resident Address:</label>
          <input
              type="text"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
          />

        </div>
        {errors.address && <span class="error-red">{errors.address}</span>}
        <div>
          <label>Phone:</label>
          <input
              type="text"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
          />

        </div>
        {errors.phone && <span class="error-red">{errors.phone}</span>}
        <div>
        <button type="submit">Submit</button>
        </div>
      </form>
  );
};

export default App;
