<?php
// Assume you have already established a connection to the MySQL database
// You can use PDO or mysqli for database connection

// Function to fetch data from the API
function fetchDataFromAPI($url) {
    // Send an HTTP request to the API endpoint
    // and retrieve the data
    $response = file_get_contents($url);

    // Decode the JSON response into an associative array
    $data = json_decode($response, true);

    return $data;
}

// Function to store data in the database
function storeDataInDatabase($name, $email, $phone, $address, $dob) {
    // Prepare the INSERT statement
    $stmt = $pdo->prepare("INSERT INTO person (name, email, phone, address, dob) VALUES (?, ?, ?, ?, ?)");

    // Bind the values to the statement parameters
    $stmt->bindParam(1, $name);
    $stmt->bindParam(2, $email);
    $stmt->bindParam(3, $phone);
    $stmt->bindParam(4, $address);
    $stmt->bindParam(5, $dob);

    // Execute the statement
    $stmt->execute();
}

// Fetch data from the API
$apiData = fetchDataFromAPI('https://api.example.com/data');

// Extract the required fields from the API data
$name = $apiData['name'];
$email = $apiData['email'];
$phone = $apiData['phone'];
$address = $apiData['address'];
$dob = $apiData['dob'];

// Store the extracted data in the database
storeDataInDatabase($name, $email, $phone, $address, $dob);

// Send a response back to the frontend
$response = [
    'success' => true,
    'message' => 'Data stored successfully'
];

header('Content-Type: application/json');
echo json_encode($response);
